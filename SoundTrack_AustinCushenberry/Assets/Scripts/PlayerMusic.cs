﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;// Added sound control

public class PlayerMusic : MonoBehaviour
{
    public AudioMixer mixer;
    public AudioMixerSnapshot[] snapshots;
    public float [] weights;
    public float tranlen;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("MusicTrig"))
        {
            weights[0] = 0.0f;
            weights[1] = 1.0f;
            mixer.TransitionToSnapshots(snapshots, weights, tranlen);

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("MusicTrig"))
        {
            weights[0] = 1.0f;
            weights[1] = 0.0f;
            mixer.TransitionToSnapshots(snapshots, weights, tranlen);

        }
    }


}

